Toptal Technical 2 Interview for QA Engineer

Task 1 - Toptal Job Creation Wizard Test Automation

Description: The tests for the Job Creation Wizard have been automated on Robot Framework RIDE using Selenium 2 Library.

This are the installation instructions to be able to run the tests:

1. Download and install the Java Development Kit. It includes the Java Runtime Environment along with the libraries needed for development. Robotframework supports custom libraries written in Java. Install the JDK if you plan on using Java libraries or Install the JRE if you don't plan on writing custom libraries in Java.
The latest version of JDK and JRE can be found on the following link: http://www.oracle.com/technetwork/java/javase/downloads/index.html. The latest version as of writing this document is Java SE 7 Update 4
2. Download and install the Python environment version 2.7.1 Note that there are two major version of Python: Python 2.7.1 and Python 3.x Currently Robotframework only supports Python 2.x, be sure to install the correct version.
The latest version of the Python 2.7.x environment can be found on the following link: http://www.python.org/download/releases/2.7.1/. For 32-bit OS download link and for 64-bit OS download: link
3. Download and install wxPython from the following location: http://sourceforge.net/projects/wxpython/files/wxPython/2.8.12.1/. Download the appropriate version for your architecture, and for Python 2.7. Make sure to pick the unicode version.
4. If running Windows, you need to update the environment variable to include the python executables, to do so follow these steps:
- Click on Start and right click on "Computer" and click on "Properties"
- Go to the "Advanced tab" and towards the bottom, click on "Environment Variables" button
- On the bottom box, locate "Path", select it and click on "Edit" button
- Add the following on the edit box: ;C:\Python27;C:\Python27\Scripts note that the path could change, please double check that this path is correct by locating the installation folder for Robotframework and Python.

5. Installing Robotframework and Selenium RC

In order to have the full Robotframework environment we need to install the following resources:

- Download and copy "ez_setup.py" to your C:\Python27\Scripts\ folder, then open your command prompt, and run "ez_setup.py". This will install easy_install.py.  Link : https://pypi.python.org/pypi/setuptools/0.7.2#windows
- Close Command Prompt and reopen. Type "easy_install pip" in command prompt to install pip.  
- Type "pip install robotframework-selenium2library==1.4.0" in command prompt to install.
- Type "pip install --upgrade robotframework-selenium2library==1.4.0" in command prompt to upgrade.

Now that you have your environment ready, you can now run the tests by following the steps:

1. Open a Command prompt, and go to the JobCreation.html file location
2. Run the tests by typing: pybot JobCreation.html

When the automated tests have finished the execution, you will get the test results on a separate html file called "report.html", and a log for all the tests on a file called "log.html", inside the same folder where JobCreation.html file is located.